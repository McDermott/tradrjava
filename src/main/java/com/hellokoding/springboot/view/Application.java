package com.hellokoding.springboot.view;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import services.utils.DatabaseVerification;

import java.sql.SQLException;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {


    public static void main(String[] args) throws SQLException {

        SpringApplication.run(Application.class, args);

        DatabaseVerification dbVerification = new DatabaseVerification();
        dbVerification.validateUserTable();
        dbVerification.validateStoreTable();

    }
}
